//
//  ViewController.swift
//  MachineLearingApp
//
//  Created by Di Monda Davide on 07/03/2019.
//  Copyright © 2019 unina. All rights reserved.
//

import UIKit
import Vision
import CoreML
import ImageIO

class MainViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var openGalleryBtn: UIButton!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var resultLabel: UILabel!
    
    var imagePicker = UIImagePickerController()
    var classIndentifier = String()
    var cont = 0
    var blurCensor = UIVisualEffectView()

    //MARK: setup for classification
    lazy var classificationRequest: VNCoreMLRequest = {
        
        do{
            
            let model = try VNCoreMLModel(for: PhobiaClassifier().model)
            
            let request = VNCoreMLRequest(model: model, completionHandler: { [weak self] request, error in
                
               self?.processClassifications(for: request, error: error)
                
            })
            
            return request

        }catch{
            fatalError("Error in classificationRequest func")
        }
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultLabel.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
//        if(self.classIndentifier == "clown" || self.classIndentifier == "spider" || self.classIndentifier == "worm"){
//            self.applyBlurFilter(imageView: self.selectedImage)
//        }
    }
    
    
    //MARK: Request classification
    func updateClassifications(for image: UIImage){

        let orientation = CGImagePropertyOrientation(image.imageOrientation)
        guard let ciImage = CIImage(image: image) else { fatalError("Unable to create \(CIImage.self) from \(image).") }
        
        DispatchQueue.global(qos: .userInitiated).async {
            let handler = VNImageRequestHandler(ciImage: ciImage, orientation: orientation)
            do {
                try handler.perform([self.classificationRequest])
            } catch {
                print("Failed to perform classification.\n\(error.localizedDescription)")
            }
        }

    }
    
    
    //MARK: Process classification
    func processClassifications(for request: VNRequest, error: Error?){
        
        DispatchQueue.main.async {
            guard let results = request.results else {
                print("Unable to classify image.\n\(error!.localizedDescription)")
                return
            }

            let classifications = results as! [VNClassificationObservation]
            
            if classifications.isEmpty {
                print("Nothing recognized.")
            } else {
                
                self.cont = 0

                let topClassifications = classifications.prefix(2)
                let descriptions = topClassifications.map { classification -> String in
                    
                    if(self.cont == 0){
                        self.classIndentifier = classification.identifier
                        print(self.classIndentifier)
                        
                        if(self.classIndentifier == "clown" || self.classIndentifier == "spider" || self.classIndentifier == "worm" || self.classIndentifier == "tripophobia" || self.classIndentifier == "snake" || self.classIndentifier == "feet"){
                            self.applyBlurFilter(imageView: self.selectedImage)
                        }
                        
                        self.cont += 1
                    }
                    
                    return String(format: "  (%.2f) %@", classification.confidence, classification.identifier)
                    
                }
                self.resultLabel.text = "Classification:\n" + descriptions.joined(separator: "\n")
            }
        }
        
    }

    //MARK: Select image from gallery
    @IBAction func openGallery(_ sender: Any) {
        
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: {
                self.blurCensor.removeFromSuperview()
            })
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage{
            selectedImage.image = pickedImage
            
            updateClassifications(for: pickedImage)
        }
        resultLabel.isHidden = false
        
        dismiss(animated: true) {
//            if(self.classIndentifier == "clown" || self.classIndentifier == "spider" || self.classIndentifier == "worm"){
//                self.applyBlurFilter(imageView: self.selectedImage)
//            }
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: blur filter
    func applyBlurFilter(imageView: UIImageView){
        
        self.blurCensor.isHidden = false

        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = imageView.bounds
        self.blurCensor = blurView
        imageView.addSubview(self.blurCensor)
        
    }
    
    
}
