//
//  CameraViewController.swift
//  MachineLearingApp
//
//  Created by Alessandro Scala on 11/03/2019.
//  Copyright © 2019 unina. All rights reserved.
//

import UIKit
import AVKit
import CoreML
import Vision

class CameraViewController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate {

    @IBOutlet weak var cameraView: UIView!
    @IBOutlet weak var predictionLabel: UILabel!
    
    var predictionText: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let captureSession = AVCaptureSession()
        captureSession.sessionPreset = .photo
        
        guard let captureDevice = AVCaptureDevice.default(for: .video) else{
            return
        }
        
        guard let input = try? AVCaptureDeviceInput(device: captureDevice) else{
            return
        }
        
        captureSession.addInput(input)
        captureSession.startRunning()
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        cameraView.layer.addSublayer(previewLayer)
        previewLayer.frame = cameraView.frame
        
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(dataOutput)
        
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (_) in
            DispatchQueue.main.async {
                self.predictionLabel.text = self.predictionText
            }
        }
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else{
            return
        }
        
        guard let model = try? VNCoreMLModel(for: PhobiaClassifier().model) else{
            return
        }
        
        let request = VNCoreMLRequest(model: model) { (request, error) in
            guard let results = request.results as? [VNClassificationObservation] else{
                return
            }
            
            self.predictionText = "\(results[0].identifier): \(results[0].confidence)\n\(results[1].identifier): \(results[1].confidence)"
        }
        
        try? VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:]).perform([request])
    }

    
}
